import java.util.Scanner;

public class AnalyticQuery {
    public static void analyticDisplay(String tableName) {
        switch (tableName) {
            case "city":
                System.out.println("4. Вывести города с населением в промежутке");
                System.out.println("5. Вывести все города, начинающиеся с определенных символов");
                break;
            case "district":
                System.out.println("4. Вывести районы с площадью в промежутке");
                System.out.println("5. Вывести районы, начинающиеся с определенных символов");
                break;
            case "pizzeria":
                System.out.println("4. Вывести пиццерии с количеством мест в промежутке");
                System.out.println("5. Вывести пиццерии, номер которых начинается с определенных символов");
                break;
            case "pizza":
                System.out.println("4. Вывести пиццы с ценой в промежутке");
                System.out.println("5. Вывести пиццы, название которых начинается с определенных символов");
                break;
            case "order":
                System.out.println("4. Вывести заказы с датой в промежутке");
                System.out.println("5. Вывести заказы, номер которых начинается с определенных символов");
                break;
            case "pizza_order":
                System.out.println("4. Показать таблицу pizza");
                System.out.println("5. Показать таблицу order\n");
            default:
                break;
        }
    }

    public static String analyticNumber(String tableName, String field) {
        String sql = "";
        Scanner scanner = new Scanner(System.in);
        int firstValue;
        int secondValue;

        System.out.println("Введите промежуток");
        System.out.println("Первое значение:");
        firstValue = scanner.nextInt();
        System.out.println("Второе значение:");
        secondValue = scanner.nextInt();
        System.out.println("Итоговое условие: вывод в промежутке " + firstValue + " и " + secondValue);
        sql = "select * From pizza_chain." + tableName +" where pizza_chain."+tableName+"." + field + " between "+firstValue+" and "+secondValue+";";

        return sql;
    }


    public static String analyticString(String tableName, String field) {
        String sql = "";
        Scanner scanner = new Scanner(System.in);
        String stringValue = "";

        System.out.println("Введите строку для поиска (без указания %)");
        stringValue = scanner.nextLine();
        stringValue += "%";
        sql = "select * from pizza_chain."+tableName+" where pizza_chain." + tableName +"."+field+" like (\""+stringValue+"\");";

        return sql;
    }
}
