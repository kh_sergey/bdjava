import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import static java.lang.System.exit;

public class SwitchBlock {
    public static boolean mainSwitch(Connection conn1, int switchNumber, String tableName) throws SQLException, IOException {
        String selectAll = "SELECT * FROM pizza_chain.";
        Scanner scanner = new Scanner(System.in);
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String sql1 = "";
        String sql2 = "";
        String field = "";
        String type = "";
        int rowId = 0;
        int rowCount = 0;
        int fieldId;
        String fieldValueString = "";
        boolean restartFlag = false;
        switch (switchNumber) {
            case 0:
                SelectData.selectAllTableFields(conn1, selectAll + tableName);
                break;
            case 1:
                SelectData.selectAllTableFields(conn1, selectAll + tableName);
                System.out.println("Выберите ПОЛЕ по которому хотите удалить запись");
                SelectData.selectOnlyFields(conn1, selectAll + tableName);
                fieldId = scanner.nextInt();
                // теперь берём название поля
                field = SelectData.selectOneField(conn1, selectAll+tableName, fieldId);
                type = SelectData.selectOneFieldType(conn1, selectAll+tableName, fieldId);
                System.out.println("Выберите ЗНАЧЕНИЕ поля, по которому хотите удалить запись");
                fieldValueString = bufferedReader.readLine();
                if (!type.equals("INT") || !type.equals("DOUBLE")) {
                    fieldValueString+="\"";
                }
                StringBuilder sb1 = new StringBuilder(fieldValueString);
                if (!type.equals("INT") || !type.equals("DOUBLE")) {
                    sb1.insert(0, "\"");
                }
                System.out.println("Выбрана запись:");
                rowCount = SelectData.selectAllTableFields(conn1, selectAll + tableName + " WHERE " + tableName + "." +field + " = " +sb1+";");
                if (rowCount == 1) {
                    System.out.println("Вы уверены, что хотите удалить эту запись? Y/N");
                    String confirmationString = bufferedReader.readLine();
                    if (confirmationString.equals("Y")) {
                        PreparedStatement st = conn1.prepareStatement("DELETE FROM pizza_chain." + tableName + " WHERE " + tableName + "." + field + " = " + sb1 + ";");
                        st.executeUpdate();
                        SelectData.selectAllTableFields(conn1, selectAll + tableName);
                        st.close();
                    } else {
                        System.out.println("Вы не согласны с удалением записи и будете возвращены в меню инструментов");
                        break;
                    }
                } else if (rowCount >= 2) {
                    System.out.println("Выбрано несколько записей. Уточните свой поиск выбором ID записи");
                    rowId = scanner.nextInt();
                    if (!tableName.equals("pizza_order")) {
                        SelectData.selectAllTableFields(conn1, selectAll + tableName + " WHERE " + tableName + ".id_" + tableName + " = " + rowId + ";");
                    } else if (tableName.equals("pizza_order")) {
                        SelectData.selectAllTableFields(conn1, selectAll + tableName + " WHERE " + tableName + ".id = " + rowId + ";");
                    }

                    System.out.println("Вы уверены, что хотите удалить эту запись? Y/N");
                    String confirmationString = bufferedReader.readLine();
                    if (confirmationString.equals("Y")) {
                        String sqlTmp = "";
                        if (!tableName.equals("pizza_order")) {
                            sqlTmp = "DELETE FROM pizza_chain." + tableName + " WHERE " + tableName + ".id_" + tableName + " = " + rowId + ";";
                        } else if (tableName.equals("pizza_order")) {
                            sqlTmp = "DELETE FROM pizza_chain." + tableName + " WHERE " + tableName + ".id = " + rowId + ";";
                        }
                        PreparedStatement st = conn1.prepareStatement(sqlTmp);
                        st.executeUpdate();
                        SelectData.selectAllTableFields(conn1, selectAll + tableName);
                        st.close();
                    } else {
                        System.out.println("Вы не согласны с удалением записи и будете возвращены в меню инструментов");
                        break;
                    }
                }
                break;
            case 2:
                SelectData.selectAllTableFields(conn1, selectAll + tableName);

                System.out.println("Выберите ПОЛЕ по которому хотите изменить запись");
                SelectData.selectOnlyFields(conn1, selectAll + tableName);
                fieldId = scanner.nextInt();
                // теперь берём название поля
                field = SelectData.selectOneField(conn1, selectAll+tableName, fieldId);
                type = SelectData.selectOneFieldType(conn1, selectAll+tableName, fieldId);
                System.out.println("Выберите ЗНАЧЕНИЕ поля, по которому хотите изменить запись");
                fieldValueString = bufferedReader.readLine();
                if (!type.equals("INT") || !type.equals("DOUBLE")) {
                    fieldValueString+="\"";
                }
                StringBuilder sb2 = new StringBuilder(fieldValueString);
                if (!type.equals("INT") || !type.equals("DOUBLE")) {
                    sb2.insert(0, "\"");
                }

                System.out.println("Выбрана запись:");
                rowCount = SelectData.selectAllTableFields(conn1, selectAll + tableName + " WHERE " + tableName + "." +field + " = " +sb2+";");
                if (rowCount == 1) {
                    //rowId = scanner.nextInt();
                    PreparedStatement statement2 = conn1.prepareStatement("SELECT * FROM pizza_chain." + tableName + " WHERE " + tableName + "." + field+" = " + sb2 + ";");
                    ResultSet data2 = statement2.executeQuery();
                    if (data2.next() == false) {
                        System.out.println("Записи не существует, попробуйте ещё раз");
                    } else {
                        int coloumnCnt = data2.getMetaData().getColumnCount();
                        String sqlVar = "UPDATE " + tableName + " SET ";
                        String sqlValues = "";
                        for (int i = 2; i <= coloumnCnt; i++) {
                            System.out.printf("Укажите значения для поля " + data2.getMetaData().getColumnName(i) + ", типа: " + data2.getMetaData().getColumnTypeName(i) + "\n");
                            if (data2.getMetaData().getColumnTypeName(i).equals("INT")) {
                                String var_varchar = bufferedReader.readLine();
                                sqlValues += var_varchar;
                            } else if (data2.getMetaData().getColumnTypeName(i).equals("VARCHAR")) {
                                String var_varchar = bufferedReader.readLine();
                                sqlValues += "\"" + var_varchar + "\"";
                            }
                            sqlVar += data2.getMetaData().getColumnName(i) + " = " + sqlValues + ",";
                            sqlValues = "";
                        }
                        statement2.close();
                        StringBuffer sb3 = new StringBuffer(sqlVar);
                        sb3.deleteCharAt(sb3.length() - 1);
                        System.out.println(sb3 + " WHERE " + tableName + "." + field + "=" + sb2 + ";");
                        PreparedStatement st = conn1.prepareStatement(sb3 + " WHERE " + tableName + "." + field + "=" + sb2 + ";");
                        st.executeUpdate();
                        SelectData.selectAllTableFields(conn1, selectAll + tableName);
                        st.close();
                    }
                } else if (rowCount >=2 ) {
                    String sqlTmp = "";
                    System.out.println("Выбрано несколько записей. Уточните свой поиск выбором ID записи");
                    rowId = scanner.nextInt();
                    if (!tableName.equals("pizza_order")) {
                        SelectData.selectAllTableFields(conn1, selectAll + tableName + " WHERE " + tableName + ".id_" + tableName + " = " + rowId + ";");
                    } else if (tableName.equals("pizza_order")) {
                        SelectData.selectAllTableFields(conn1, selectAll + tableName + " WHERE " + tableName + ".id = " + rowId + ";");
                    }

                    if (!tableName.equals("pizza_order")) {
                        sqlTmp = "SELECT * FROM pizza_chain." + tableName + " WHERE " + tableName + ".id_" + tableName + " = " + rowId + ";";
                    } else if (tableName.equals("pizza_order")) {
                        sqlTmp = "SELECT * FROM pizza_chain." + tableName + " WHERE " + tableName + ".id = " + rowId + ";";
                    }

                    PreparedStatement statement2 = conn1.prepareStatement(sqlTmp);

                    ResultSet data2 = statement2.executeQuery();
                    if (data2.next() == false) {
                        System.out.println("Записи не существует, попробуйте ещё раз");
                    } else {
                        int coloumnCnt = data2.getMetaData().getColumnCount();
                        String sqlVar = "UPDATE " + tableName + " SET ";
                        String sqlValues = "";
                        for (int i = 2; i <= coloumnCnt; i++) {
                            System.out.printf("Укажите значения для поля " + data2.getMetaData().getColumnName(i) + ", типа: " + data2.getMetaData().getColumnTypeName(i) + "\n");
                            if (data2.getMetaData().getColumnTypeName(i).equals("INT")) {
                                String var_varchar = bufferedReader.readLine();
                                sqlValues += var_varchar;
                            } else if (data2.getMetaData().getColumnTypeName(i).equals("VARCHAR")) {
                                String var_varchar = bufferedReader.readLine();
                                sqlValues += "\"" + var_varchar + "\"";
                            }
                            sqlVar += data2.getMetaData().getColumnName(i) + " = " + sqlValues + ",";
                            sqlValues = "";
                        }
                        statement2.close();
                        StringBuffer sb3 = new StringBuffer(sqlVar);
                        sb3.deleteCharAt(sb3.length() - 1);

                        if (!tableName.equals("pizza_order")) {
                            System.out.println(sb3 + " WHERE " + tableName + ".id_" + tableName + "=" + rowId + ";");
                            sqlTmp = sb3 + " WHERE " + tableName + ".id_" + tableName + "=" + rowId + ";";
                        } else if (tableName.equals("pizza_order")) {
                            System.out.println(sb3 + " WHERE " + tableName + ".id_" + tableName + "=" + rowId + ";");
                            sqlTmp = sb3 + " WHERE " + tableName + ".id =" + rowId + ";";
                        }

                        PreparedStatement st = conn1.prepareStatement(sqlTmp);
                        st.executeUpdate();
                        SelectData.selectAllTableFields(conn1, selectAll + tableName);
                        st.close();
                    }
                }
                break;
            case 3:
                SelectData.selectAllTableFields(conn1, selectAll + tableName);
                System.out.println("Сейчас вы будете вводить данные для своей новой записи");
                PreparedStatement statement3 = conn1.prepareStatement("SELECT * FROM pizza_chain." + tableName);
                ResultSet data3 = statement3.executeQuery();
                int coloumnCnt = data3.getMetaData().getColumnCount();

                String sqlVar = "INSERT INTO pizza_chain." + tableName + " (";
                String sqlValues = "VALUES (";
                for (int i = 2; i <= coloumnCnt; i++) {
                    System.out.printf("Укажите значения для поля " + data3.getMetaData().getColumnName(i) + ", типа: " + data3.getMetaData().getColumnTypeName(i) + "\n");
                    if (data3.getMetaData().getColumnTypeName(i).equals("INT") || data3.getMetaData().getColumnTypeName(i).equals("DOUBLE")) {
                        String var_varchar = bufferedReader.readLine();
                        sqlValues += var_varchar + ",";
                    } else if (data3.getMetaData().getColumnTypeName(i).equals("VARCHAR") || data3.getMetaData().getColumnTypeName(i).equals("DATE") || data3.getMetaData().getColumnTypeName(i).equals("TIME")) {
                        String var_varchar = bufferedReader.readLine();
                        sqlValues += "\"" + var_varchar + "\",";
                    }
                    sqlVar += data3.getMetaData().getColumnName(i) + ",";
                }
                statement3.close();
                StringBuffer sb12 = new StringBuffer(sqlVar);
                StringBuffer sb22 = new StringBuffer(sqlValues);
                sb12.deleteCharAt(sb12.length() - 1);
                sb22.deleteCharAt(sb22.length() - 1);
                System.out.println(sb12);
                System.out.println(sb22);
                System.out.println(sb12 + ")" + sb22 + ")");
                PreparedStatement st = conn1.prepareStatement(sb12 + ")" + sb22 + ")");
                st.executeUpdate();
                SelectData.selectAllTableFields(conn1, selectAll + tableName);
                st.close();
                break;
            case 4:
                if (!tableName.equals("pizza_order")) {
                    field = SwitchBlock.fieldSwitchNumber(tableName);
                    sql1 = AnalyticQuery.analyticNumber(tableName, field);
                    SelectData.selectAllTableFields(conn1, sql1);
                } else if (tableName.equals("pizza_order")) {
                    SelectData.selectAllTableFields(conn1, selectAll+"pizza");
                }
                break;
            case 5:
                if (!tableName.equals("pizza_order")) {
                    field = SwitchBlock.fieldSwitchString(tableName);
                    sql2 = AnalyticQuery.analyticString(tableName, field);
                    SelectData.selectAllTableFields(conn1, sql2);
                } else if (tableName.equals("pizza_order")) {
                    SelectData.selectAllTableFields(conn1, selectAll+"order");
                }
                break;
            case 9:
                restartFlag = true;
                break;
            case 10:
                System.out.println("Спасибо за использование нашей программы, всего доброго");
                conn1.close();
                exit(0);
            default:
                System.out.println("Введите число в промежутке 0-5 или 9 для выбора другой таблицы");
                break;
        }
        return restartFlag;
    }

    public static String fieldSwitchNumber(String tableName){
        String field = "";

        switch (tableName) {
            case "city":
                field = "population";
                break;
            case "district":
                field = "district_area";
                break;
            case "pizzeria":
                field = "seats_number";
                break;
            case "pizza":
                field = "price";
                break;
            case "order":
                field = "delivery_date";
                break;
            default:
                break;
        }

        return field;
    }

    public static String fieldSwitchString(String tableName){
        String field = "";

        switch (tableName) {
            case "city":
                field = "name";
                break;
            case "district":
                field = "name";
                break;
            case "pizzeria":
                field = "number_pizzeria";
                break;
            case "pizza":
                field = "name";
                break;
            case "order":
                field = "number";
                break;
            default:
                break;
        }

        return field;
    }
}
