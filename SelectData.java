import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class SelectData {
    public static int selectAllTableFields(Connection conn1, String sql) throws SQLException {
        PreparedStatement statement = conn1.prepareStatement(sql);
        ResultSet data = statement.executeQuery();
        int coloumnCnt = data.getMetaData().getColumnCount();
        int count = 0;

        for (int i = 1;i<=coloumnCnt;i++){
            System.out.printf("%-30s", data.getMetaData().getColumnName(i)+": "+data.getMetaData().getColumnTypeName(i));
        }

        while (data.next()){
            System.out.println();
            for(int i = 1;i<=coloumnCnt;i++) {
                System.out.printf("%-30s",data.getString(i));
            }
            count++;
        }
        System.out.println("\n");
        statement.close();
        return count;
    }

    public static void selectOnlyFields(Connection conn1, String sql) throws SQLException{
        PreparedStatement statement = conn1.prepareStatement(sql);
        ResultSet data = statement.executeQuery();
        int coloumnCnt = data.getMetaData().getColumnCount();

        for (int i = 2;i<=coloumnCnt;i++){
            System.out.printf(i+". "+data.getMetaData().getColumnName(i)+": "+data.getMetaData().getColumnTypeName(i)+"\n");
        }
        System.out.println("\n");
        statement.close();
    }

    public static String selectOneField(Connection conn1, String sql, int fieldNumber) throws SQLException{
        PreparedStatement statement = conn1.prepareStatement(sql);
        ResultSet data = statement.executeQuery();

        System.out.print(data.getMetaData().getColumnName(fieldNumber)+", ");

        return data.getMetaData().getColumnName(fieldNumber);
    }

    public static String selectOneFieldType(Connection conn1, String sql, int fieldNumber) throws SQLException{
        PreparedStatement statement = conn1.prepareStatement(sql);
        ResultSet data = statement.executeQuery();

        System.out.print(data.getMetaData().getColumnTypeName(fieldNumber)+"\n");

        return data.getMetaData().getColumnName(fieldNumber);
    }
}
