import java.io.*;
import java.sql.*;
import java.util.Scanner;

import static java.lang.System.exit;

class MySQLConnectExample {
    public static void main(String[] args) {

        Connection conn1 = null;
        boolean restartFlag = false;
        int rowId;
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        Scanner scanner = new Scanner(System.in);

        try {
            String url1 = "jdbc:mysql://localhost:3306/pizza_chain?useUnicode=true&serverTimezone=UTC";
            String user = "root";
            String password = "141014genh";
            String selectAll = "SELECT * FROM ";

            conn1 = DriverManager.getConnection(url1, user, password);

            if (conn1 != null) {
                System.out.println("Connected to the database pizza_chain");

                while (true) {
                    DisplayClass.mainMenuDisplay();
                    // пока выводим только 2 таблицы и те ручками

                /*
                // ПОЛНЫЙ ПЕРЕБОР
                DatabaseMetaData md = conn1.getMetaData();
                ResultSet rs = md.getTables(null, null, "%", null);
                while (rs.next()) {
                    System.out.println(rs.getString("TABLE_NAME"));
                }
                 */

                    String tableName = bufferedReader.readLine();

                    if (!tableName.equals("city") && !tableName.equals("district") && !tableName.equals("pizzeria") && !tableName.equals("pizza")
                    && !tableName.equals("pizza_order") && !tableName.equals("order")) {
                        System.out.println("Указана неверная таблица для работы");
                        continue;
                    }

                    while (true) {
                        restartFlag = false;

                        DisplayClass.toolsMenuDisplay(tableName);

                        int switchNumber = scanner.nextInt();

                        restartFlag = SwitchBlock.mainSwitch(conn1, switchNumber, tableName);
                        if (restartFlag == true) {
                            break;
                        }
                    }
                }
            }

        } catch (SQLException | IOException ex) {
            System.out.println("An error occurred. Maybe user/password is invalid");
            ex.printStackTrace();
        }
    }
}