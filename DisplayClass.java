import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DisplayClass {
    public static void mainMenuDisplay() {
        System.out.println("------------------------------ DATABASE MAIN MENU -----------------------------------");
        System.out.println("Введите название таблицы, с которой хотите работать: ");
        System.out.println("Выберите: city, district, pizzeria, pizza, pizza_order(MTM), order");
    }

    public static void toolsMenuDisplay(String tableName) {
        System.out.println("------------------------------ DATABASE TOOLS MENU ----------------------------------");
        System.out.println("Выберите что вы хотите сделать:");
        System.out.println("0. Просмотреть таблицу ");
        System.out.println("1. Удалить запись в таблице ");
        System.out.println("2. Изменить значение записи в таблице");
        System.out.println("3. Добавить запись в таблицу\n");
        if (!tableName.equals("pizza_order")) {
            System.out.println("////////////////////////////// Аналитические запросы ///////////////////////////////");
            AnalyticQuery.analyticDisplay(tableName);
            System.out.println("////////////////////////////////////////////////////////////////////////////////////\n");
        } else if (tableName.equals("pizza_order")) {
            AnalyticQuery.analyticDisplay(tableName);
        }
        System.out.println("9. Выбрать другую таблицу");
        System.out.println("10. Выйти из программы");
        System.out.println("------------------------------------------------------------------------------------");
    }
}
